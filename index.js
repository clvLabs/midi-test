const midi = require('midi');
const chalk = require('chalk');

const SELECTED_MIDI_PORT = 2;

const notes = [
  'C ',
  'C#',
  'D ',
  'D#',
  'E ',
  'F ',
  'F#',
  'G ',
  'G#',
  'A ',
  'A#',
  'B ',
];

function showData(data) {
  const spaces = '        ';
  process.stdout.write(data + spaces + '\r');
}

function onMIDIMessage(deltaTime, message) {
  // https://www.cs.cf.ac.uk/Dave/Multimedia/node158.html 
  const msg = message[0];
  const key = message[1];
  const val = message[2];

  const KEY_PRESS = 144;
  const KEY_RELEASE = 128;

  const noteIndex = key % 12;
  const octave = Math.floor(key / 12);

  let text = `${notes[noteIndex]} ${octave} (${key}) [val: ${val}] [msg: ${msg}]`;

  if (msg == KEY_PRESS) {
    text = chalk.green(text);
  } else if (msg === KEY_RELEASE) {
    text = chalk.blue(text);
  } else {
    text = chalk.yellow(text);
  }

  showData(text);

  if (msg === KEY_RELEASE && noteIndex === 0 && octave === 6) {
    console.log('\n');
    console.log(chalk.red.bold('C 6 released!'));
    console.log(chalk.red.bold('Exiting program...'));
    input.closePort();
  }
}

// ------------------------------------------------------------------

console.log(chalk.white('---[ MIDI test ]----------------------'));

console.log('Starting MIDI connector....');
const input = new midi.input();
input.ignoreTypes(false, false, false);  // Order: (Sysex, Timing, Active Sensing) 
input.on('message', onMIDIMessage);
const portCount = input.getPortCount();

console.log(`Available ports (${portCount}):`);
for (let index = 0; index < portCount; index++) {
  const portName = input.getPortName(index);
  let label = `- ${portName}`;

  if (index === SELECTED_MIDI_PORT) {
    label = chalk.white(label);
  }

  console.log(label);
}


console.log(`Opening selected port: (${input.getPortName(SELECTED_MIDI_PORT)}):`);
input.openPort(SELECTED_MIDI_PORT);

// ... receive MIDI messages ...
console.log('\nPress some keys....');
console.log('(C 6 release will exit the program)\n');
